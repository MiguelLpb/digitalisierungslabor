Codes zu unterschiedlichen Teilprojekte des Digitalisierungslabors an der Hochschule Fulda
-> GUI für Touchscreen
-> Python Code für den Betrieb eines Hochregallagers
-> Scratch Code für ein fahrerloses Transportsystem (Robomaster S1) mit Linienerkennung

Für mehr Information über das Projekt: https://www.hs-fulda.de/unsere-hochschule/alle-meldungen/meldungsdetails/detail/studierende-bauen-komplexes-hochregallager-nach-1
