# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Startbildschirm.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!
import  revpimodio2
import sys, time
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import QTimer, QEventLoop, Qt
from PyQt5.QtWidgets import QApplication, QLabel, QVBoxLayout


hrlNamen =["A1","A2","A3","A4","A5","B1","B2","B3","B4","B5","C1","C2","C3","C4","C5","D1","D2","D3","D4","D5"]
hrlState= [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


class Ui_Startbildschirm(object):
    def setupUi(self, Startbildschirm):
        Startbildschirm.setObjectName("Startbildschirm")
        Startbildschirm.resize(1920, 1080)
        Startbildschirm.setMinimumSize(QtCore.QSize(1920, 1080))
        Startbildschirm.setMaximumSize(QtCore.QSize(1920, 1080))
        Startbildschirm.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        Startbildschirm.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint) #Öffnet Fenster im Vollbildmodus

        self.hslogo = QtWidgets.QLabel(Startbildschirm)
        self.hslogo.setGeometry(QtCore.QRect(750, 50, 500, 112))
        self.hslogo.setStyleSheet("background-image: url(hsfuldabanner.png); background.repeat: no-repeat")
        self.hslogo.setObjectName("hslogo")

        self.TitleText = QtWidgets.QLabel(Startbildschirm)
        self.TitleText.setGeometry(QtCore.QRect(0, -200, 1920, 1080))
        self.TitleText.setAlignment(Qt.AlignCenter)
        self.TitleText.setStyleSheet("font-size: 75px; font-weight: bold")
        self.TitleText.setObjectName("TitleText")
        self.TitleText.setText(QtCore.QCoreApplication.translate("Startbildschirm", "Hochregallager"))

        self.signature = QtWidgets.QLabel(Startbildschirm)
        self.signature.setGeometry(QtCore.QRect(0, 500, 1920, 1080))
        self.signature.setAlignment(Qt.AlignCenter)
        self.signature.setStyleSheet("font-size: 15px; font-weight: bold")
        self.signature.setObjectName("signature")
        self.signature.setText(QtCore.QCoreApplication.translate("Startbildschirm", "Alla Cherdantseva, Klaudia Stuczynska, Tabea Heil, Miguel Leñero, Levin Franz und Lukas Isaak"))

        self.StartButton = QtWidgets.QPushButton(Startbildschirm)
        self.StartButton.setGeometry(QtCore.QRect(730, 500, 483, 231))
        self.StartButton.setObjectName("StartButton")
        self.StartButton.clicked.connect(self.showMatrix)
        self.retranslateUi(Startbildschirm)
        QtCore.QMetaObject.connectSlotsByName(Startbildschirm)

        self.ExitButton = QtWidgets.QPushButton(Startbildschirm)
        self.ExitButton.setGeometry(QtCore.QRect(850, 830, 241, 116))
        self.ExitButton.setObjectName("ExitButton")
        self.ExitButton.clicked.connect(QApplication.instance().quit)

    def retranslateUi(self, Startbildschirm):
        _translate = QtCore.QCoreApplication.translate
        Startbildschirm.setWindowTitle(_translate("Startbildschirm", "Dialog"))
        self.StartButton.setText(_translate("Startbildschirm", ""))

    def showMatrix(self):
         Matrix.show()
         Startbildschirm.hide()


class Ui_Matrix(object):
    def setupUi(self, Matrix):
        Matrix.setObjectName("Matrix")
        Matrix.resize(1920, 1080)
        Matrix.setMinimumSize(QtCore.QSize(1920, 1080))
        Matrix.setMaximumSize(QtCore.QSize(1920, 1080))
        Matrix.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)  # Öffnet Fenster im Vollbildmodus
        self.A1 = QtWidgets.QPushButton(Matrix)
        self.A1.setGeometry(QtCore.QRect(304, 150, 228, 152))
        self.A1.setObjectName("A1")
        self.A2 = QtWidgets.QPushButton(Matrix)
        self.A2.setGeometry(QtCore.QRect(608, 150, 228, 152))
        self.A2.setObjectName("A2")
        self.A3 = QtWidgets.QPushButton(Matrix)
        self.A3.setGeometry(QtCore.QRect(912, 150, 228, 152))
        self.A3.setObjectName("A3")
        self.A4 = QtWidgets.QPushButton(Matrix)
        self.A4.setGeometry(QtCore.QRect(1216, 150, 228, 152))
        self.A4.setObjectName("A4")
        self.A5 = QtWidgets.QPushButton(Matrix)
        self.A5.setGeometry(QtCore.QRect(1520, 150, 228, 152))
        self.A5.setObjectName("A5")
        self.B1 = QtWidgets.QPushButton(Matrix)
        self.B1.setGeometry(QtCore.QRect(304, 320, 228, 152))
        self.B1.setObjectName("B1")
        self.B2 = QtWidgets.QPushButton(Matrix)
        self.B2.setGeometry(QtCore.QRect(608, 320, 228, 152))
        self.B2.setObjectName("B2")
        self.B3 = QtWidgets.QPushButton(Matrix)
        self.B3.setGeometry(QtCore.QRect(912, 320, 228, 152))
        self.B3.setObjectName("B3")
        self.B4 = QtWidgets.QPushButton(Matrix)
        self.B4.setGeometry(QtCore.QRect(1216, 320, 228, 152))
        self.B4.setObjectName("B4")
        self.B5 = QtWidgets.QPushButton(Matrix)
        self.B5.setGeometry(QtCore.QRect(1520, 320, 228, 152))
        self.B5.setObjectName("B5")
        self.C1 = QtWidgets.QPushButton(Matrix)
        self.C1.setGeometry(QtCore.QRect(304, 490, 228, 152))
        self.C1.setObjectName("C1")
        self.C2 = QtWidgets.QPushButton(Matrix)
        self.C2.setGeometry(QtCore.QRect(608, 490, 228, 152))
        self.C2.setObjectName("C2")
        self.C3 = QtWidgets.QPushButton(Matrix)
        self.C3.setGeometry(QtCore.QRect(912, 490, 228, 152))
        self.C3.setObjectName("C3")
        self.C4 = QtWidgets.QPushButton(Matrix)
        self.C4.setGeometry(QtCore.QRect(1216, 490, 228, 152))
        self.C4.setObjectName("C4")
        self.C5 = QtWidgets.QPushButton(Matrix)
        self.C5.setGeometry(QtCore.QRect(1520, 490, 228, 152))
        self.C5.setObjectName("C5")
        self.D1 = QtWidgets.QPushButton(Matrix)
        self.D1.setGeometry(QtCore.QRect(304, 660, 228, 152))
        self.D1.setObjectName("D1")
        self.D2 = QtWidgets.QPushButton(Matrix)
        self.D2.setGeometry(QtCore.QRect(608, 660, 228, 152))
        self.D2.setObjectName("D2")
        self.D3 = QtWidgets.QPushButton(Matrix)
        self.D3.setGeometry(QtCore.QRect(912, 660, 228, 152))
        self.D3.setObjectName("D3")
        self.D4 = QtWidgets.QPushButton(Matrix)
        self.D4.setGeometry(QtCore.QRect(1216, 660, 228, 152))
        self.D4.setObjectName("D4")
        self.D5 = QtWidgets.QPushButton(Matrix)
        self.D5.setGeometry(QtCore.QRect(1520, 660, 228, 152))
        self.D5.setObjectName("D5")
        self.backToStartMenu = QtWidgets.QPushButton(Matrix)
        self.backToStartMenu.setGeometry(QtCore.QRect(906, 860, 242, 116))
        self.backToStartMenu.setObjectName("backToStartMenu")


        #Knopf Funktionen

        self.A1.clicked.connect(lambda: self.showEin_Auslagern("A1"))
        self.A2.clicked.connect(lambda: self.showEin_Auslagern("A2"))
        self.A3.clicked.connect(lambda: self.showEin_Auslagern("A3"))
        self.A4.clicked.connect(lambda: self.showEin_Auslagern("A4"))
        self.A5.clicked.connect(lambda: self.showEin_Auslagern("A5"))
        self.B1.clicked.connect(lambda: self.showEin_Auslagern("B1"))
        self.B2.clicked.connect(lambda: self.showEin_Auslagern("B2"))
        self.B3.clicked.connect(lambda: self.showEin_Auslagern("B3"))
        self.B4.clicked.connect(lambda: self.showEin_Auslagern("B4"))
        self.B5.clicked.connect(lambda: self.showEin_Auslagern("B5"))
        self.C1.clicked.connect(lambda: self.showEin_Auslagern("C1"))
        self.C2.clicked.connect(lambda: self.showEin_Auslagern("C2"))
        self.C3.clicked.connect(lambda: self.showEin_Auslagern("C3"))
        self.C4.clicked.connect(lambda: self.showEin_Auslagern("C4"))
        self.C5.clicked.connect(lambda: self.showEin_Auslagern("C5"))
        self.D1.clicked.connect(lambda: self.showEin_Auslagern("D1"))
        self.D2.clicked.connect(lambda: self.showEin_Auslagern("D2"))
        self.D3.clicked.connect(lambda: self.showEin_Auslagern("D3"))
        self.D4.clicked.connect(lambda: self.showEin_Auslagern("D4"))
        self.D5.clicked.connect(lambda: self.showEin_Auslagern("D5"))
        self.backToStartMenu.clicked.connect(self.showStartMenu)

        #Knopf Beschriftung
        self.retranslateUi(Matrix)
        QtCore.QMetaObject.connectSlotsByName(Matrix)

    def retranslateUi(self, Matrix):
        _translate = QtCore.QCoreApplication.translate
        Matrix.setWindowTitle(_translate("Matrix", "Hochregallager"))
        self.A1.setText(_translate("Matrix", "A1"))
        self.A2.setText(_translate("Matrix", "A2"))
        self.A3.setText(_translate("Matrix", "A3"))
        self.A4.setText(_translate("Matrix", "A4"))
        self.A5.setText(_translate("Matrix", "A5"))
        self.B1.setText(_translate("Matrix", "B1"))
        self.B2.setText(_translate("Matrix", "B2"))
        self.B3.setText(_translate("Matrix", "B3"))
        self.B4.setText(_translate("Matrix", "B4"))
        self.B5.setText(_translate("Matrix", "B5"))
        self.C1.setText(_translate("Matrix", "C1"))
        self.C2.setText(_translate("Matrix", "C2"))
        self.C3.setText(_translate("Matrix", "C3"))
        self.C4.setText(_translate("Matrix", "C4"))
        self.C5.setText(_translate("Matrix", "C5"))
        self.D1.setText(_translate("Matrix", "D1"))
        self.D2.setText(_translate("Matrix", "D2"))
        self.D3.setText(_translate("Matrix", "D3"))
        self.D4.setText(_translate("Matrix", "D4"))
        self.D5.setText(_translate("Matrix", "D5"))
        self.backToStartMenu.setText(_translate("Matrix", ""))

    def showEin_Auslagern(self,Lagerplatz):
        print(Lagerplatz)


        if hrlState[hrlNamen.index(Lagerplatz)] == 0:
            print("Frei")
            ui3.einlagernauslagern.setText(QtCore.QCoreApplication.translate("EinlagernAuslagern", ""))
            ui3.einlagernauslagern.setStyleSheet("background-image:url(einlagernbuttonsmall.png)")

            ui3.einlagernauslagern.clicked.connect(lambda: einlagern(Lagerplatz))

            #einlagern(Lagerplatz)
        else:
            print("Besetzt")
            ui3.einlagernauslagern.setText(QtCore.QCoreApplication.translate("EinlagernAuslagern", ""))
            ui3.einlagernauslagern.setStyleSheet("background-image:url(auslagernbuttonsmall.png)")
            ui3.einlagernauslagern.clicked.connect(lambda: auslagern(Lagerplatz))
            #auslagern(Lagerplatz)
        ui3.Lagerplatz.setText(QtCore.QCoreApplication.translate("EinlagernAuslagern", Lagerplatz))

        EinlagernAuslagern.show()
        Matrix.hide()

    def showStartMenu(self):

        Startbildschirm.show()
        Matrix.hide()


class Ui_EinlagernAuslagern(object):
    def setupUi(self, EinlagernAuslagern):
        EinlagernAuslagern.setObjectName("EinlagernAuslagern")
        EinlagernAuslagern.resize(1920, 1080)
        EinlagernAuslagern.setMinimumSize(QtCore.QSize(1920, 1080))
        EinlagernAuslagern.setMaximumSize(QtCore.QSize(1920, 1080))
        EinlagernAuslagern.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)  # Öffnet Fenster im Vollbildmodus
        self.Lagerplatz = QtWidgets.QLabel(EinlagernAuslagern)
        self.Lagerplatz.setGeometry(QtCore.QRect(920, 240, 131, 51))
        font = QtGui.QFont()
        font.setPointSize(30)
        self.Lagerplatz.setFont(font)
        self.Lagerplatz.setObjectName("Lagerplatz")
        self.einlagernauslagern = QtWidgets.QPushButton(EinlagernAuslagern)
        self.einlagernauslagern.setGeometry(QtCore.QRect(760, 350, 362, 173))
        self.einlagernauslagern.setObjectName("einlagernauslagern")
        self.backtoMatrix = QtWidgets.QPushButton(EinlagernAuslagern)
        self.backtoMatrix.setGeometry(QtCore.QRect(820, 600, 242, 116))
        self.backtoMatrix.setObjectName("backtoMatrix")


        self.backtoMatrix.clicked.connect(self.showMatrix)

        self.retranslateUi(EinlagernAuslagern)
        QtCore.QMetaObject.connectSlotsByName(EinlagernAuslagern)

    def retranslateUi(self, EinlagernAuslagern):
        _translate = QtCore.QCoreApplication.translate
        EinlagernAuslagern.setWindowTitle(_translate("EinlagernAuslagern", "Hochregallager"))
        self.Lagerplatz.setText(_translate("EinlagernAuslagern", "TextLabel"))
        self.einlagernauslagern.setText(_translate("EinlagernAuslagern", "Einlagern"))
        self.backtoMatrix.setText(_translate("EinlagernAuslagern", ""))

    def showMatrix(self):
        ui3.einlagernauslagern.clicked.disconnect()
        Matrix.show()
        EinlagernAuslagern.hide()



class Ui_LoadingScreen(object):
    def setupUi(self, LoadingScreen):
        LoadingScreen.setObjectName("LoadingScreen")
        LoadingScreen.resize(1920, 1080)
        LoadingScreen.setMinimumSize(QtCore.QSize(1920, 1080))
        LoadingScreen.setMaximumSize(QtCore.QSize(1920, 1080))
        LoadingScreen.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)  # Öffnet Fenster im Vollbildmodus
        LoadingScreen.setStyleSheet("background-image: url(loadingscreenbackground.png); background-position: center")



        self.Lagerplatz = QtWidgets.QLabel(LoadingScreen)
        self.Lagerplatz.setGeometry(QtCore.QRect(0, 200, 1920, 1080))
        self.Lagerplatz.setAlignment(Qt.AlignCenter)
        self.Lagerplatz.setStyleSheet("font-size: 50px")
        self.Lagerplatz.setObjectName("Lagerplatz")

        self.SchriftOben = QtWidgets.QLabel(LoadingScreen)
        self.SchriftOben.setGeometry(QtCore.QRect(0, -200, 1920, 1080))
        self.SchriftOben.setAlignment(Qt.AlignCenter)
        self.SchriftOben.setStyleSheet("font-size: 50px")
        self.SchriftOben.setObjectName("SchriftOben")

        self.SchriftMitte = QtWidgets.QLabel(LoadingScreen)
        self.SchriftMitte.setGeometry(QtCore.QRect(0, 0, 1920, 1080))
        self.SchriftMitte.setAlignment(Qt.AlignCenter)
        self.SchriftMitte.setStyleSheet("font-size: 50px")
        self.SchriftMitte.setObjectName("SchriftMitte")

        self.retranslateUi(LoadingScreen)
        QtCore.QMetaObject.connectSlotsByName(LoadingScreen)

    def retranslateUi(self, LoadingScreen):
        _translate = QtCore.QCoreApplication.translate
        LoadingScreen.setWindowTitle(_translate("LoadingScreen", "Hochregallager"))
        self.Lagerplatz.setText(_translate("LoadingScreen", "Lagerplatz"))
        self.SchriftOben.setText(_translate("LoadingScreen", "Schrift oben"))
        self.SchriftMitte.setText(_translate("LoadingScreen", "Schrift Mitte"))




def einlagern(Lagerplatz):
    ui3.einlagernauslagern.clicked.disconnect()
    ui4.SchriftOben.setText(QtCore.QCoreApplication.translate("LoadingScreen", "Einlagern"))
    ui4.SchriftMitte.setText(QtCore.QCoreApplication.translate("LoadingScreen", Lagerplatz+" wird eingelagert..."))
    ui4.Lagerplatz.setText(QtCore.QCoreApplication.translate("LoadingScreen", Lagerplatz))
    LoadingScreen.show()
    EinlagernAuslagern.hide()
    #print(Lagerplatz)

    #Laufband
    rpi.io.MEingang.value=1
    loop = QEventLoop()
    QTimer.singleShot(2000, loop.quit)
    loop.exec_()
    rpi.io.MEingang.value=0

    #Ganz nach links fahren
    while rpi.io.SEingang.value==0 :
        rpi.io.M5Rechts.value = 1
    rpi.io.M5Links.value = 0

    #Arm runter
    while rpi.io.SDUnten.value == 0:
        rpi.io.M3Runter.value = 1
    rpi.io.M3Runter.value = 0

    if Lagerplatz=="A1":
        ui2.A1.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")

        while rpi.io.S1.value==0 :
            rpi.io.M5Rechts.value = 1
        rpi.io.M5Rechts.value = 0
        while rpi.io.SAOben.value==0 :
            rpi.io.M3Hoch.value = 1
        rpi.io.M3Hoch.value = 0
        absetzen()


    elif Lagerplatz=="A2":
         ui2.A2.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="A3":
         ui2.A3.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="A4":
        ui2.A4.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="A5":
        ui2.A5.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="B1":
        ui2.B1.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="B2":
        ui2.B2.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="B3":
        ui2.B3.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="B4":
        ui2.B4.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="B5":
        ui2.B5.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="C1":
        ui2.C1.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="C2":
        ui2.C2.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "C3":
        ui2.C3.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "C4":
        ui2.C4.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="C5":
        ui2.C5.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="D1":
        ui2.D1.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "D2":
        ui2.D2.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "D3":
        ui2.D3.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "D4":
        ui2.D4.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz=="D5":
        ui2.D5.setStyleSheet("background-image:url(besetztbutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    #Matrix Feld setzen (voll oder leer)
    hrlState[hrlNamen.index(Lagerplatz)]=1

    Matrix.show()
    LoadingScreen.hide()


def auslagern(Lagerplatz):
    ui3.einlagernauslagern.clicked.disconnect()
    ui4.SchriftOben.setText(QtCore.QCoreApplication.translate("LoadingScreen", "Auslagern"))
    ui4.SchriftMitte.setText(QtCore.QCoreApplication.translate("LoadingScreen", Lagerplatz + " wird ausgelagert..."))
    ui4.Lagerplatz.setText(QtCore.QCoreApplication.translate("LoadingScreen", Lagerplatz))
    LoadingScreen.show()
    EinlagernAuslagern.hide()
    #print(Lagerplatz)
    loop = QEventLoop()
    QTimer.singleShot(2000, loop.quit)
    loop.exec_()
    if Lagerplatz == "A1":
        ui2.A1.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "A2":
        ui2.A2.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "A3":
        ui2.A3.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "A4":
        ui2.A4.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "A5":
        ui2.A5.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "B1":
        ui2.B1.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "B2":
        ui2.B2.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "B3":
        ui2.B3.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "B4":
        ui2.B4.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "B5":
        ui2.B5.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "C1":
        ui2.C1.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "C2":
        ui2.C2.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "C3":
        ui2.C3.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "C4":
        ui2.C4.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "C5":
        ui2.C5.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "D1":
        ui2.D1.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "D2":
        ui2.D2.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "D3":
        ui2.D3.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "D4":
        ui2.D4.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    elif Lagerplatz == "D5":
        ui2.D5.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
        # Matrix Feld setzen (voll oder leer)
    hrlState[hrlNamen.index(Lagerplatz)] = 0
    Matrix.show()
    LoadingScreen.hide()



def init_color():

    ui.StartButton.setStyleSheet("background-image: url(startbutton.png); background-attachment: fixed")
    ui.ExitButton.setStyleSheet("background-image: url(exitbutton.png); background-attachment: fixed")

    ui2.A1.setStyleSheet("background-image: url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.A2.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.A3.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.A4.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.A5.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.B1.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.B2.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.B3.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.B4.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.B5.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.C1.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.C2.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.C3.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.C4.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.C5.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.D1.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.D2.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.D3.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.D4.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.D5.setStyleSheet("background-image:url(freibutton.png); background-attachment: fixed; font-weight: bold; font-size: 30px; color: white")
    ui2.backToStartMenu.setStyleSheet("background-image: url(abbrechenbuttonsmall.png); background-attachment: fixed")
    ui3.backtoMatrix.setStyleSheet("background-image: url(abbrechenbuttonsmall.png); background-attachment: fixed")

def absezten():
    print("Ware wird abgesetzt")

app = QtWidgets.QApplication(sys.argv)

Startbildschirm = QtWidgets.QDialog()
ui = Ui_Startbildschirm()
ui.setupUi(Startbildschirm)

Matrix = QtWidgets.QDialog()
ui2=Ui_Matrix()
ui2.setupUi(Matrix)


EinlagernAuslagern = QtWidgets.QDialog()
ui3 = Ui_EinlagernAuslagern()
ui3.setupUi(EinlagernAuslagern)

LoadingScreen= QtWidgets.QDialog()
ui4 = Ui_LoadingScreen()
ui4.setupUi(LoadingScreen)

init_color()

try:
    rpi=revpimodio2.RevPiModIO(autorefresh=True)
except Exception as e:
    print(e)


Startbildschirm.show()
sys.exit(app.exec_())
