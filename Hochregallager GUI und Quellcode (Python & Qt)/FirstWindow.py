# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'FirstWindow.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from SecondWindow import *

class Ui_FirstWindow(object):

    def openWindow(self):
        self.window = QtWidgets.QDialog()

        self.lagerplatz = "A1"
        self.ui = Ui_SecondWindow(self.lagerplatz)

        self.ui.setupUi(self.window)
        FirstWindow.hide()
        self.window.show()
    def setupUi(self, FirstWindow):
        FirstWindow.setObjectName("FirstWindow")
        FirstWindow.resize(400, 300)
        font = QtGui.QFont()
        font.setPointSize(15)
        FirstWindow.setFont(font)
        self.pushButton = QtWidgets.QPushButton(FirstWindow)
        self.pushButton.setGeometry(QtCore.QRect(110, 100, 171, 91))
        self.pushButton.setObjectName("pushButton")

        self.pushButton.clicked.connect(self.openWindow)

        self.retranslateUi(FirstWindow)
        QtCore.QMetaObject.connectSlotsByName(FirstWindow)

    def retranslateUi(self, FirstWindow):
        _translate = QtCore.QCoreApplication.translate
        FirstWindow.setWindowTitle(_translate("FirstWindow", "FirstWindow"))
        self.pushButton.setText(_translate("FirstWindow", "PushButton"))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    FirstWindow = QtWidgets.QDialog()
    ui = Ui_FirstWindow()
    ui.setupUi(FirstWindow)
    FirstWindow.show()
    sys.exit(app.exec_())
